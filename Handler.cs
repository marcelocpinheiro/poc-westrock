using westrock.modules.createUser;

[assembly:LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace AwsDotnetCsharp;
public class Handler
{
    public Response User(Request request)
    {
        var response = (new CreateUserHandler()).handle(new CreateUserRequest(request.Username));
        return new Response(response.Message, request);
    }

    public Response Permission(Request request)
    {
        return new Response("Serverless 2", request);
    }
}

public class Response
{
  public string Message {get; set;}
  public Request Request {get; set;}

  public Response(string message, Request request)
  {
    Message = message;
    Request = request;
  }
}
// Aqui � poss�vel definir uma interface de request para a classe handler
// Principal e outro para o m�dulo. Assim voc� separa o input do usu�rio
// do objeto a ser tratado pelo sistema. Isso permite fazer valida��es de forma
// mais semantica e clara, alem de facilitar nos testes.
public class Request
{
  public string Username {get; set;}
}
