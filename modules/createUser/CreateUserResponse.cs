﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace westrock.modules.createUser
{
    public class CreateUserResponse
    {
        public string Message { get; set; }
    }
}
