﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace westrock.modules.createUser
{
    class CreateUserRequest
    {
        public CreateUserRequest(string username) { 
            this.UserName = username;
        }
        public string UserName { get; set; }
    }
}
