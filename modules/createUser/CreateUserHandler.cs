﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace westrock.modules.createUser
{
    class CreateUserHandler
    {
        public CreateUserResponse handle(CreateUserRequest request)
        {
            var response = new CreateUserResponse();
            response.Message = $"Usuário {request.UserName} criado com sucesso";
            return response;
        }
    }
}
