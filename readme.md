# Westrock serverless framework template

## Setup

- [Instale o node com NPM](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-and-create-a-local-development-environment-on-windows)
- [Instale o serverless-cli](https://www.serverless.com/console/docs)
- [Instale o docker e o docker-compose](https://docs.docker.com/desktop/install/windows-install/)
- ** [Configure o AWS-CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html) com todas as permissões relacionadas ao cloud-formation, cloudwatch e lambda. Sem ele, o deploy não acontece.
- Instale as dependências necessárias no terminal na pasta raíz:
```bash
npm install
```
- No terminal, na pasta raíz do projeto, rode o seguinte comando, que irá subir o localstack na sua máquina e rodar o docker em background:
```bash
docker-compose up -d
```

- Rode o build do projeto para que ele gere os artefatos necessários
```bash
bash build.sh
# para sistemas windows, ele também tem o arquivo build.bat
```

- Agora, é necessário rodar um "Deploy local" das functions para que seja possivel testá-las localmente:
```bash
serverless deploy --stage local
# Se você quiser, você pode fazer o deploy de apenas uma função utilizando o parametros -f
```
- Quando a execução anterior for finalizada, rode o seguinte comando para subir a estrutura que hospeda as functions localmente:
```bash
serverless invoke --stage local -f [NOME DA FUNCAO QUE QUER EXECUTAR]
```

O nome da função é o que é especificado no arquivo serverless.yml.
Para ter controle sobre como passar parametros de qualquer natureza para a lambda, verifique a [documentação do serverless framework sobre o comando Invoke](https://www.serverless.com/framework/docs/providers/aws/cli-reference/invoke-local)

## Deploy

Cada ambiente é chamado de Stage. O Serverless framework está configurado para fazer um deploy para cada function usando o seguinte padrão: `[NOME_DO_PROJETO]-[STAGE]-[FUNCAO]`. Por exemplo, uma função com o nome "user" vai ser implantada na aws com o nome "westrock-dev-user"

Para fazer o deploy no ambiente desejado, basta rodar o comando:
```bash
serverless deploy --stage [STAGE]
```
O framework vai gerar um novo conjunto de todas as functions para o stage especificado. Se elas ja estiverem criadas, ele vai apenas substituir.

Para fazer o deploy de uma função específica, utiliza a flag `--function`
```bash
serverless deploy --stage [STAGE] --function [FUNCAO]
```
O nome da função a ser utilizado aqui é o nome declarado no arquivo serverless.yml

Idealmente, cria-se os stages de DEV, QA e PROD e cria-se uma entrada de API-Gateway ára cada stage.

